package com.myc.demo.service;

/**
 * @auther 木玉川/ybs_0921@163.com
 * @date 2018/12/23 9:37
 */
public interface IDemoService {

    String getName(String name);

}
