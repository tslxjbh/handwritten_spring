package com.myc.demo.service.impl;

import com.myc.demo.service.IDemoService;
import com.myc.demo.servletFrame.annotation.MYCService;

/**
 * @auther 木玉川/ybs_0921@163.com
 * @date 2018/12/23 10:01
 */
@MYCService
public class DemoServiceImpl implements IDemoService {
    @Override
    public String getName(String name) {
        return "my Name is =:" + name;
    }
}
