package com.myc.demo.action;

import com.myc.demo.service.IDemoService;
import com.myc.demo.servletFrame.annotation.MYCAutowired;
import com.myc.demo.servletFrame.annotation.MYCController;
import com.myc.demo.servletFrame.annotation.MYCRequestMapping;
import com.myc.demo.servletFrame.annotation.MYCRequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @auther 木玉川/ybs_0921@163.com
 * @date 2018/12/23 9:39
 */
@MYCController
@MYCRequestMapping("/demo")
public class DemoAction {

    @MYCAutowired
    private IDemoService iDemoService;

    @MYCRequestMapping("/query.json")
    public void query(HttpServletRequest req, HttpServletResponse resp,
                      @MYCRequestParam("name") String name){
        String result = iDemoService.getName(name);
        try {
            resp.getWriter().write(result);
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    @MYCRequestMapping("/add.json")
    public void add(HttpServletRequest req, HttpServletResponse resp,@MYCRequestParam("a")Integer a,
                    @MYCRequestParam("b")Integer b){
        try {
            resp.getWriter().write(a+"+"+b+"="+(a+b));
        }catch (IOException e){
            e.printStackTrace();
        }
    }


    /**
     * 初始化加载配置
     * @param req
     * @param resp
     * @param id
     */
    @MYCRequestMapping("/remove.json")
    public void remove(HttpServletRequest req, HttpServletResponse resp,@MYCRequestParam("id")Integer id){




    }

}
