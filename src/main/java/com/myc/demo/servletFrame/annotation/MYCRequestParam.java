package com.myc.demo.servletFrame.annotation;

import java.lang.annotation.*;

/**
 * @auther 木玉川/ybs_0921@163.com
 * @date 2018/12/23 9:55
 */

@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MYCRequestParam {
    String value() default "";
}
